<?php
return [
    'host'     => env('API1C_HOST', '127.0.0.1'),
    'base'     => env('API1C_BASE', 'base'),
    'user'     => env('API1C_LOGIN', ''),
    'password' => env('API1C_PASSWORD', ''),
    'ssl'      => env('API1C_SSL', 'true'),
];