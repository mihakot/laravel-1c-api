<?php

namespace Mihakot\Laravel1C;

use Illuminate\Support\Traits\Macroable;
use Mihakot\Laravel1C\Objects\Catalog;

class API1C
{
    use Macroable {
        __call as macroCall;
    }

    /**
     * Execute a method against a new pending request instance.
     *
     * @param string $method
     * @param array  $parameters
     *
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        if (static::hasMacro($method)) {
            return $this->macroCall($method, $parameters);
        }

        //        if(class_exists($method)) {
        //            return app($method);
        //        }
    }

    public function Catalog($entity)
    {
        return app(Catalog::class, ['entity' => $entity]);
    }
}
