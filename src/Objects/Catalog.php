<?php

namespace Mihakot\Laravel1C\Objects;

use GuzzleHttp\Exception\GuzzleException;
use Mihakot\Laravel1C\Client\Client;
use Mihakot\Laravel1C\Interfaces\API1CInterface;

class Catalog implements API1CInterface
{
    private string $entity;

    private Client $client;

    /**
     * @param string $entity
     */
    public function __construct(string $entity)
    {
        $this->entity = "Catalog_".$entity;
        $this->client = new Client();
    }

    /**
     * @return \Illuminate\Support\Collection
     * @throws GuzzleException
     */
    public function get(): \Illuminate\Support\Collection
    {
        return collect($this->client->request($this->entity));
    }

    public function post(){

    }
}